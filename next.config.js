const path = require('path');
const { IgnorePlugin } = require('webpack');


module.exports = {
  env: {},

  webpack(config, { isServer }) {
    if (!isServer) config.resolve.alias['@sentry/node'] = '@sentry/browser';

    config.resolve.extensions = ['.tsx', 'ts', '.wasm', '.mjs', '.jsx', '.js', '.json'];
    config.plugins.push(new IgnorePlugin(/_spec\.[tj]sx?$/));

    Object.assign(config.resolve.alias, {
      components: path.resolve(__dirname, 'components'),
      queries: path.resolve(__dirname, 'queries'),
      themes: path.resolve(__dirname, 'themes'),
    });

    return config;
  }
};