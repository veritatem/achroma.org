type HashtagPropTypes = {
    value: String,
};

export default function Hashtag({ value }: HashtagPropTypes) {
    const encoded = encodeURIComponent('#' + value);
    const target = "_twitter_" + value;

    return (
        <>
            <a target={target} href="https://twitter.com/search?q=&src=typed_query&f=live">
                { value }
            </a>

            <style jsx>{`
                a {
                    position: relative;
                    top: 0.1ex;
                    padding: 0.1ex 0.1em;
                    display: inline-block;
                    background-color: #F90;
                    color: #333;
                    font-weight: bold;
                    border-radius: 0.4ex;
                }

                a::before { content: '#'; }
            `}</style>
        </>
    );
}
