import Head from 'next/head'

export default function Layout({ children }) {
  return (
    <>
      <Head>
        <title>achroma</title>
        <link rel="shortcut icon" href="/assets/favicon/favicon.ico" />
      </Head>

      <main>{ children }</main>

      <footer>
        this is{' '}
        <span className="achroma-text">achroma</span>
      </footer>

      <style jsx>{`
        main {
          margin: 0 auto;
          max-width: 34em;
          padding-bottom: 1rem;
        }
      `}</style>
    </>
  );
}